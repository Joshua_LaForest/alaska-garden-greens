from django.db import models
from app_accounts.models import Category, Customer

# Create your models here.
class Produce(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
    description = models.CharField(
        max_length=250, default='', blank=True, null=True)
    quantity = models.SmallIntegerField(blank=True)
    image = models.ImageField(upload_to = "images/", null = True, blank = True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default = 7)
    
    
    def get_products_by_id(ids):
        return Produce.objects.filter(id__in=ids)

    @staticmethod
    def get_all_products():
        return Produce.objects.all()

    @staticmethod
    def get_all_products_by_categoryid(category_id):
        if category_id:
            return Produce.objects.filter(category=category_id)
        else:
            return Produce.get_all_products()