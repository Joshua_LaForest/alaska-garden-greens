# Generated by Django 4.1 on 2022-08-08 23:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='produce',
            name='image',
            field=models.ImageField(blank=True, upload_to=''),
        ),
    ]
