# Generated by Django 4.1 on 2022-08-10 15:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_accounts', '0002_customer'),
        ('app_main', '0005_produce_price_alter_produce_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='produce',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app_accounts.category'),
        ),
    ]
