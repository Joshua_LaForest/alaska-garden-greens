from django.shortcuts import render, redirect, get_object_or_404
from .models import Produce
from django.contrib.auth.decorators import user_passes_test
from .forms import ProduceForm

# Create your views here.
def list_of_products(request):
    produce = Produce.objects.all()
    return render(request, "main/all_produce.html", {"produce": produce})


def product_details(request, pk):
    produce = Produce.objects.filter(pk=pk)
    context = {
        "produce": produce
    }
    return render(request, "main/produce_details.html", context)

@user_passes_test(lambda u: u.is_superuser)
def create_product(request):
    context = {}
    if request.method == "POST":
        form = ProduceForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect("home")
        else:
            form = ProduceForm
            context["form"] = form
    return render(request, "main/create.html", context)

@user_passes_test(lambda u: u.is_superuser)
def update_product(request, pk):
    context = {}
    obj = get_object_or_404(Produce, pk=pk)
    form = ProduceForm(request.POST or None, instance=obj)
    if form.is_valid():
        produce = form.save()
        return redirect("details", produce.pk)
    context ["form"] = form
    return render(request, "main/update.html", context)